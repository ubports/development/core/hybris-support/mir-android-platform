Source: mir-android-platform
Section: x11
Priority: optional
Maintainer: UBports Developers <developers@ubports.com>
Build-Depends: cmake,
               cmake-extras,
               debhelper-compat (= 13),
               doxygen,
               xsltproc,
               graphviz,
               libegl1-mesa-dev,
               libgles2-mesa-dev,
               libglm-dev,
               libprotobuf-dev,
               pkg-config,
               libgoogle-glog-dev,
               android-headers-24,
               android-headers-24-caf,
               android-headers,
               libhardware-dev [i386 amd64 armhf arm64],
               libandroid-properties-dev [i386 amd64 armhf arm64],
               libgtest-dev,
               googletest | google-mock (>= 1.6.0+svn437),
# only enable valgrind once it's been tested to work on each architecture:
               valgrind [amd64 i386 armhf arm64],
               abi-compliance-checker,
               python3:any,
               libmir1common-dev,
               libmir1platform-dev,
               libmir1core-dev,
               libmir1renderer-dev,
               libmir1server-dev,
               libmir1client-dev,
               libmir1al-dev,
               mir1-renderer-gl-dev,
               mir1test-dev,
               libcapnp-dev,
               liblttng-ust-dev,
               libudev-dev,
               libglib2.0-dev,
               libumockdev-dev (>= 0.6),
               umockdev (>= 0.8.7),
               libwayland-dev
Standards-Version: 3.9.4
Homepage: https://gitlab.com/ubports/development/core/hybris-support/mir-android-platform
Vcs-Git: https://gitlab.com/ubports/development/core/hybris-support/mir-android-platform.git
Vcs-Browser: https://gitlab.com/ubports/development/core/hybris-support/mir-android-platform

Package: mir1-android-tests
Architecture: i386 amd64 armhf arm64
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends},
         ${shlibs:Depends},
Recommends: mir-demos,
Description: Display Server for Ubuntu - android platform diagnostics utility
 Mir is a display server running on linux systems, with a focus on efficiency,
 robust operation and a well-defined driver model.
 .
 Contains unit tests

Package: mir1-platform-graphics-android16
Section: libs
Architecture: i386 amd64 armhf arm64
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends},
         ${shlibs:Depends},
Description: Display server for Ubuntu - platform library for Android
 Mir is a display server running on linux systems, with a focus on efficiency,
 robust operation and a well-defined driver model.
 .
 Contains the shared libraries required for the Mir server to interact with
 the hardware platform using the Android drivers.

Package: mir1-platform-graphics-android-caf16
Section: libs
Architecture: i386 amd64 armhf arm64
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends},
         ${shlibs:Depends},
Description: Display server for Ubuntu - platform library for Android (caf)
 Mir is a display server running on linux systems, with a focus on efficiency,
 robust operation and a well-defined driver model.
 .
 Contains the shared libraries required for the Mir server to interact with
 the hardware platform using the Android (caf) drivers.

Package: mir1-client-platform-android5
Section: libs
Architecture: i386 amd64 armhf arm64
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends},
         ${shlibs:Depends}
Description: Display server for Ubuntu - client platform library for Android
 Mir is a display server running on linux systems, with a focus on efficiency,
 robust operation and a well-defined driver model.
 .
 Contains the shared libraries required for the Mir clients to interact with
 the underlying hardware platform using the Android drivers.

Package: mir1-client-platform-android-caf5
Section: libs
Architecture: i386 amd64 armhf arm64
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends},
         ${shlibs:Depends}
Description: Display server for Ubuntu - client platform library for Android (caf)
 Mir is a display server running on linux systems, with a focus on efficiency,
 robust operation and a well-defined driver model.
 .
 Contains the shared libraries required for the Mir clients to interact with
 the underlying hardware platform using the Android (caf) drivers.

Package: mir1-graphics-drivers-android
Section: libs
Architecture: i386 amd64 armhf arm64
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends},
         mir1-platform-graphics-android16,
         mir1-client-platform-android5,
         mir1-platform-input-evdev7,
Description: Display server for Ubuntu - android driver metapackage
 Mir is a display server running on linux systems, with a focus on efficiency,
 robust operation and a well-defined driver model.
 .
 This package depends on a full set of graphics drivers for running Mir on top
 of an existing Android driver stack.

Package: mir1-graphics-drivers-android-caf
Section: libs
Architecture: i386 amd64 armhf arm64
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends},
         mir1-platform-graphics-android-caf16,
         mir1-client-platform-android-caf5,
         mir1-platform-input-evdev7,
Description: Display server for Ubuntu - android (caf) driver metapackage
 Mir is a display server running on linux systems, with a focus on efficiency,
 robust operation and a well-defined driver model.
 .
 This package depends on a full set of graphics drivers for running Mir on top
 of an existing Android (caf) driver stack.
